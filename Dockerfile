FROM mesosphere/aws-cli
RUN apk update && apk --update add python3 \
    && apk add --upgrade terraform \
    && apk del terraform \
    && apk --no-cache add curl \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && rm /var/cache/apk/*
RUN curl -O https://releases.hashicorp.com/terraform/1.1.9/terraform_1.1.9_linux_amd64.zip \
    && unzip terraform_1.1.9_linux_amd64.zip -d /usr/local/bin/ \
    && chmod +x /usr/local/bin/terraform
ENTRYPOINT ["/bin/sh"]
